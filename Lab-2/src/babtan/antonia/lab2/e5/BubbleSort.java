package babtan.antonia.lab2.e5;
import java.util.Random;

public class BubbleSort {
    public static void main(String[] args) {
        int i, j, temp;

        Random r = new Random();

        int[] a = new int[10];

        for (i = 0; i < a.length; i++) {
            a[i] = r.nextInt(30);
        }
        System.out.println("The initial generated vector:");
        for (i = 0; i < a.length; i++) {
            System.out.print("a[" + i + "]=" + a[i] + " ");
        }

        int n = a.length;
        for (i = 0; i < n - 1; i++)
            for (j = 0; j < n - i - 1; j++)
                if (a[j] > a[j + 1]) {
                    temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }

        System.out.println("\nThe sorted vector:");

        for (i = 0; i < a.length; i++) {
            System.out.print("a[" + i + "]=" + a[i] + " ");
        }
    }
}