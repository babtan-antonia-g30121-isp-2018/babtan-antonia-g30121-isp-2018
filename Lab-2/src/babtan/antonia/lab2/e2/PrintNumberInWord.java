package babtan.antonia.lab2.e2;
import java.util.Scanner;


public class PrintNumberInWord {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a number:");
        int x = in.nextInt();

        String number;

        if (x == 1) {
            number = "ONE";
        } else if (x == 2) {
            number = "TWO";
        } else if (x == 3) {
            number = "THREE";
        } else if (x == 4) {
            number = "FOUR";
        } else if (x == 5) {
            number = "FIVE";
        } else if (x == 6) {
            number = "SIX";
        } else if (x == 7) {
            number = "SEVEN";
        } else if (x == 8) {
            number = "EIGHT";
        } else if (x == 9) {
            number = "NINE";
        } else {
            number = "OTHER";
        }
        System.out.println("NUMBER = " + number);
    }
}
