package babtan.antonia.lab2.e7;

import java.util.Random;
import java.util.Scanner;

public class GuesTheNumber {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner in = new Scanner(System.in);
        int trials = 0, number, guess;
        number = rand.nextInt(20);
        do {
            System.out.println("Enter your guess: ");
            guess = in.nextInt();
            if (number < guess) {
                System.out.println("Wrong answer,your number is too high!");
            } else if (number > guess) {
                System.out.println("Wrong answer,your number is too low!");
            } else {
                System.out.println("Congratulations!");
            }
            trials++;
        } while (trials != 3);
        System.out.println("You lost!\nThe number is: " + number);
    }
}
