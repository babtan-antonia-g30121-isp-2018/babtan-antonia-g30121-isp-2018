package babtan.antonia.lab11.e1;

public class TemperatureController {
    TemperatureSensor t;
    TemperatureTextView tview;
    public TemperatureController(TemperatureSensor t, TemperatureTextView tview){
        t.addObserver(tview);
        this.t = t;
        this.tview = tview;

    }

}
