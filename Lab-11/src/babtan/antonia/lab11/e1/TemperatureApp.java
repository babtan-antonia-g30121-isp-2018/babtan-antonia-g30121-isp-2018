package babtan.antonia.lab11.e1;

import java.awt.BorderLayout;
import javax.swing.*;


public class TemperatureApp  extends  JFrame{
    TemperatureApp(TemperatureTextView tview){
        setLayout(new BorderLayout());
        add(tview,BorderLayout.NORTH);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        TemperatureSensor t = new TemperatureSensor();
        t.start();

        TemperatureTextView tview = new TemperatureTextView();
        TemperatureController tcontroler = new TemperatureController(t,tview);

        new TemperatureApp(tview);
    }
}