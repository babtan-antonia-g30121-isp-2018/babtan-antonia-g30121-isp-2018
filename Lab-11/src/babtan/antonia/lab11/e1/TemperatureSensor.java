package babtan.antonia.lab11.e1;

import java.util.Observable;
import java.util.Random;

public class TemperatureSensor extends Observable implements Runnable {
    private double temp=30;
    private Thread t;
    private Random r=new Random();

    public void start(){
        if(t==null){
            t = new Thread(this);
            t.start();
        }
    }

    public void run() {
        while (true) {
            double d = r.nextDouble() * 8 + 21;
            temp = d;
            this.setChanged();
            this.notifyObservers();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }


    public double getTemperature(){
        return temp;
    }


}
