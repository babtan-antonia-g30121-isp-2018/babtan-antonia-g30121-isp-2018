package babtan.antonia.lab11.e2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class View extends JFrame implements Observer {
    private List<Product> products;
    private JMenuBar menuBar;
    private JMenu add, list, delete, change;
    private JPanel addinf, listing, deleting, changing;
    private JTextField name, quatity, price;
    private JTextArea text;

    public View() {
        setTitle("Inventory Management");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(250, 250);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);
        add = new JMenu("add");
        add.addActionListener(new MenuSetting());
        list = new JMenu("list");
        list.addActionListener(new MenuSetting());
        delete = new JMenu("delete");
        delete.addActionListener(new MenuSetting());
        change = new JMenu("change");
        change.addActionListener(new MenuSetting());

        menuBar.add(add);
        menuBar.add(list);
        menuBar.add(delete);
        menuBar.add(change);
        this.add(menuBar);

    }
}
