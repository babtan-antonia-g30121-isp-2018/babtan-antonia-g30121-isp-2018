package babtan.antonia.lab11.e2;

public class Product {
    private  String name;
    private int quantity;
    private double price;

    public Product(String name,int quantity,double price){
        this.name=name;
        this.quantity=quantity;
        this.price=price;

    }

    public void changeQuantity(int quantity){
        this.quantity=quantity;
    }

    public String getName(){
        return name;
    }

    public int getQuantity(){
        return quantity;
    }

    public double getPrice(){
        return price;
    }
}
