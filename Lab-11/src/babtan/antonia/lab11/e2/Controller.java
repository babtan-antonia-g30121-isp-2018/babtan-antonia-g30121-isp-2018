package babtan.antonia.lab11.e2;

public class Controller {

    public class Controller {
        Products products;
        View view;

        public  Controller(Products products,View view){
            products.addObserver(view);
            this.products=products;
            this.view=view;
        }
    }

}
