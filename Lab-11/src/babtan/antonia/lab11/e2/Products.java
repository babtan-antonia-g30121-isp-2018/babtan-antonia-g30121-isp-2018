package babtan.antonia.lab11.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Products extends Observable {
    private List<Product>products=new ArrayList<Product>();

    public void addProdusct(String name,int quantity,double price){
        Product product=new Product(name, quantity, price);
        products.add(product);
        this.setChanged();
        this.notifyObservers();
    }

    public void deleteProduct(String name){
        for(Product product:products){
            if(product.getName().equals(name));
            products.remove(product);
        }
        this.setChanged();
        this.notifyObservers();
    }

    public void changeQuantity(String name, int quantity) {
        for (Product product : products) {
            if (product.getName().equals(name))
                product.changeQuantity(quantity);
        }
        this.setChanged();
        this.notifyObservers();
    }

    public List<Product>getProducts(){
        return products;
    }
}
