package babtan.antonia.lab4.e2;

public class Author {
    private String name;
    private String email;
    private char gender;


    public Author(String n, String e, char g) {
        this.name = n;
        this.email = e;
        if ((g == 'f') || (g == 'm')) {
            this.gender = g;
        } else {
            System.err.println("Error!!");
        }
        System.out.println("\nName: " + n + "\nEmail: " + e + "\nGender: " + g);

    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public  String toString(){
        return "   "+name+" ("+gender+") "+email;
    }


}
