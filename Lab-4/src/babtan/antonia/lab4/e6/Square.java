package babtan.antonia.lab4.e6;

public class Square extends Rectangle {
    public Square() {
    }


    public Square(double side){
        super.setLength(side);
        super.setWidth(side);
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled);
        super.setLength(side);
        super.setWidth(side);
    }


    public double getSide() {
        return super.getLength();
    }

    public void setSide(double side) {
        this.setLength(side);
        this.setWidth(side);
    }

    public void setWidth(double side) {
        super.setWidth(side);
    }

    public void setLength(double side) {
        super.setLength(side);
    }

    public String toString() {
        return "A Square with side=" + this.getSide() + ", which is a subclass of" + super.toString();
    }
}
