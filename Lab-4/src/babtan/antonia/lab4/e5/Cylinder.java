package babtan.antonia.lab4.e5;

import babtan.antonia.lab4.e1.Circle;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
        System.out.println("This constructor does nothing!!");
    }

    public Cylinder(double r) {
        super(r);
    }

    public Cylinder(double r, double height) {
        super(r);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getArea() {
        return 2 * 3.14 * super.getRadius() * this.height + 2 * super.getArea();
    }

    public double getVolume() {
        return super.getArea() * this.height;
    }
}
