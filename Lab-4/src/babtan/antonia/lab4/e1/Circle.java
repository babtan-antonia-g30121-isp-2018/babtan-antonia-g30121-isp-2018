package babtan.antonia.lab4.e1;



public class Circle {
    private double radius = 1.0;
    private String color = "red";


    public Circle(double r) {
        this.radius = r;
    }

    public Circle() {
        this.color = "blue";
        this.radius = 12.4;

    }

    public double getRadius() {
        return radius;
    }

    public double getArea(){
        return 3.14*radius*radius;
    }

    public static void main(String[] args) {

    }
}