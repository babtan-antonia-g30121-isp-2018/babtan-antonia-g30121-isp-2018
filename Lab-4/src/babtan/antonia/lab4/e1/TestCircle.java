package babtan.antonia.lab4.e1;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("The first circle has the length of the radius " + c1.getRadius());
        Circle c2 = new Circle(2);
        System.out.println("The second circle has the length of the radius " + c2.getRadius());
        Circle c3 = new Circle(3);
        System.out.println(
                "The second circle has the length of the radius  " + c3.getRadius());
        System.out.println("The area of c1 = " + c1.getArea());
        System.out.println("The area of c2= " + c2.getArea());
        System.out.println("The area of c3= " + c3.getArea());
    }
}
