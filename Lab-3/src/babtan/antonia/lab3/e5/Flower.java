package babtan.antonia.lab3.e5;


public class Flower {
    static int count=0;
    int petal;
    Flower(int p){
        petal=p;
        count++;
        System.out.println("New flower has been created!");
    }
    static void Count(){
        System.out.println("Number of objects created: "+count);
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        Flower f3 = new Flower(8);
        Flower f4 = new Flower(1);
        Flower f5 = new Flower(5);
        Flower f6 = new Flower(9);
        Count();
    }



}
