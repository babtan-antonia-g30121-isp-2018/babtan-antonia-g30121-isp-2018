package babtan.antonia.lab3.e1;

public class Sensor {
    static int value;

    Sensor() {
        value = -1;
    }

    void change(int k) {
        value = value + k;
        System.out.println("Noua valoare:" + value);

    }

    void tooString() {
        System.out.println("Valoarea senzorului: " + value);
    }


}
