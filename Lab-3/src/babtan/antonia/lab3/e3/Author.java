package babtan.antonia.lab3.e3;

public class Author {
    private String name;
    private String email;
    private char gender;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public Author(String n, String e, char g) {
        name = n;
        email = e;
        if ((g == 'f')||(g == 'm')){

            this.gender = g;
        }
        else {
            System.err.println("Error!!");
        }

        System.out.println("\nName: "+n);
        System.out.println("\nEmail: "+e);
        System.out.println("\nGender: "+g);

}
        public String toString(){

        return "\n "+name+ " "+"("+gender+")"+"\nat " + email;


    }
}
