package babtan.antonia.lab3.e4;

public class MyPoint {
    int x;
    int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
        System.out.println("Point: ("+this.x+","+this.y+"}");
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Point: ("+x+","+y+"}");
    }

    public int getX()
    {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y){
        this.x=x;
        this.y=y;
    }

    public double Distance(int x2, int y2){

        return java.lang.Math.sqrt((x2-this.x)*(x2-this.x)+(y2-this.y)*(y2-this.y));


    }

    public double Distance( MyPoint another)
    {
        return java.lang.Math.sqrt((this.x-another.x)*(this.x-another.x)+(this.y-another.y)*(this.y-another.y));
    }

    public String toString(){
        return "\n("+x+","+y+")\n";
    }
}
