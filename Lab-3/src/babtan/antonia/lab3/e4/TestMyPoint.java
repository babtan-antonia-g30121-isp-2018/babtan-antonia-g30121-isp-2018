package babtan.antonia.lab3.e4;

public class TestMyPoint {

    public static void main(String[] args) {


        MyPoint p1 = new MyPoint(2, 3);
        MyPoint p2 = new MyPoint(4, 5);
        MyPoint p3 = new MyPoint(5, 1);


        double a = p1.Distance(8, 9);
        System.out.println("Distance from p1 to (8,9): " + a);

        p2.setXY(5, 4);
        int c = p2.getX();
        int d = p2.getY();
        System.out.println();
        double e = p1.Distance(p2);
        System.out.println("Distance from p1 to p2: "+e);
    }
}
