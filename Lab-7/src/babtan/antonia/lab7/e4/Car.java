package babtan.antonia.lab7.e4;

import java.io.Serializable;
import java.util.Scanner;

public class Car implements Serializable {
    private String model;
    private int price;

    public Car(){

    }

    public Car(String model,int price){
        this.model=model;
        this.price=price;

    }

    public int getPrice(){
        return price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model){
        this.model=model;
    }

    public void setPrice(int price){
        this.price=price;
    }

    public String toString(){
        return "Car typre: "+this.model+ "\nPrice:"+this.price;
    }

    public void addCar(){
        Scanner read=new Scanner(System.in);
        System.out.println("Model:");
        model=read.nextLine();
        System.out.println("Price:");
        price= read.nextInt();
        Car myCar=new Car(model,price);
    }
    public String showCar(){
        return "Car typre: "+this.model+"\nPrice:"+this.price;
    }
}
