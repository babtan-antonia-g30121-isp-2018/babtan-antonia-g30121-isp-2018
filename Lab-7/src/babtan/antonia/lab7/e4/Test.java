package babtan.antonia.lab7.e4;
import java.io.*;
import java.util.Scanner;

public class Test {
    public static void main (String[] args) throws Exception {
        String filename;
        Car a = new Car ();
        Scanner read = new Scanner (System.in);
        int opt = -1;
        while (opt != 6) {
            System.out.println ("\n\n\n1)Add car\n2)Show car\n3)Save Car Objects\n4)View Existing Car Objects\n5)Read and Display\n6)Exit\n\n\n");
            System.out.println ("Alegeti optiunea: ");
            opt = read.nextInt ();
            switch (opt) {
                case 1:
                    a.addCar ();
                    break;
                case 2:
                    System.out.println (a.showCar ());
                    break;
                case 3:
                    FileOutputStream fos = null;
                    ObjectOutputStream out = null;
                    try {
                        System.out.println ("Save with the next name(.ser) :");
                        filename = read.next ();
                        if (filename.endsWith (".ser")) {
                            fos = new FileOutputStream (filename);
                            out = new ObjectOutputStream (fos);
                            out.writeObject (a);
                            out.close ();
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                    break;
                case 4:
                    File folder = new File ("C:\\Users\\Acer\\Documents\\oancea-sergiu-g30121-isp-2018\\Lab-7\\src\\oancea\\sergiu\\lab7\\data.txt");
                    File[] listOfFiles = folder.listFiles ();
                    for (File file : listOfFiles) {
                        if (file.getName ().endsWith (".ser")) {
                            System.out.print (" " + file.getName () + "   ");
                        }
                    }
                    break;
                case 5:
                    FileInputStream fis = null;
                    ObjectInputStream in = null;
                    try {
                        System.out.println ("Get the next filename(.ser): ");
                        filename = read.next ();
                        if (filename.endsWith (".ser")) {
                            fis = new FileInputStream (filename);
                            in = new ObjectInputStream (fis);
                            a = (Car) in.readObject ();
                            in.close ();
                            System.out.println (a);
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                    break;
                case 6:
                    break;
                default:
                    System.out.println ("Invalid");
            }
        }

    }
}
