package babtan.antonia.lab7.e1;

public class PredefinedException extends Exception {
    int n;

    public PredefinedException(int n, String msg) {
        super(msg);
        this.n = n;
    }

    int getPred() {
        return n;
    }
    
}
