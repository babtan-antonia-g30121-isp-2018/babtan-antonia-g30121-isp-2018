package babtan.antonia.lab7.e1;

public class CofeeDrinker extends Exception {
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, PredefinedException {
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Cofee is to hot!");
        if (c.getConc() > 50)
            throw new ConcentrationException(c.getConc(), "Cofee concentration to high!");
        System.out.println("Drink cofee:" + c);

        if (c.getPred() > 10) {
            throw  new PredefinedException(c.getPred(),"Too much cofee!");

        }


    }
}
