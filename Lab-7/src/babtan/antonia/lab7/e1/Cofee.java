package babtan.antonia.lab7.e1;

public class Cofee {
    private int temp;
    private int conc;
    private int pred;

    Cofee(int t, int c,int n) {
        temp = t;
        conc = c;
        pred=n;

    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    int getPred(){
        return pred;
    }
    public String toString() {
        return "[Cofee temperature=" + temp + ":concentration=" + conc + " predefined number of cofees: " +pred+"]";
    }
}
