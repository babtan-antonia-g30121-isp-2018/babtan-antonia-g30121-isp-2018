package babtan.antonia.lab7.e3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class WriteInFile {
    public void write() throws Exception {
        try {
            File file = new File("data2.txt");
            PrintWriter w = new PrintWriter("data2.txt");
            Scanner read = new Scanner(System.in);
            System.out.println("Add text:");
            String line = read.nextLine();
            w.print(line);
            w.close();

        } catch (FileNotFoundException e) {
            System.err.println("Error");
        }
    }
}
