package babtan.antonia.lab7.e3;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws Exception {
        EncryptDecrypt a = new EncryptDecrypt();
        int opt = -1;
        while (opt != 3) {
            System.out.println("1.Encrypt");
            System.out.println("2.Decrypt");
            System.out.println("3.Exit");
            System.out.println("Option: ");
            Scanner r = new Scanner(System.in);
            opt = r.nextInt();
            switch (opt) {
                case 1:
                    a.encrypt();
                    break;
                case 2:
                    a.decrypt();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Invalid");
            }
        }
    }
}
