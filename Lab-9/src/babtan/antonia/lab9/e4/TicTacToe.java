package babtan.antonia.lab9.e4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe extends JPanel {
    JButton buttons[] = new JButton[9];
    int alternate = 0;

    public TicTacToe() {
        setLayout(new GridLayout(3, 3));
        initializebuttons();
    }

    public void initializebuttons() {
        for (int i = 0; i <= 8; i++) {
            buttons[i] = new JButton();
            buttons[i].setText("");
            buttons[i].addActionListener(new buttonListener());
            add(buttons[i]);
        }
    }

    public void resetButtons() {
        for (int i = 0; i <= 8; i++) {
            buttons[i].setText("");
        }
    }


    private class buttonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            JButton buttonClicked = (JButton) e.getSource(); //get the particular button that was clicked
            if (alternate % 2 == 0) {
                buttonClicked.setFont(new Font("Courier New", Font.BOLD, 30));
                buttonClicked.setText("X");
            } else {
                buttonClicked.setFont(new Font("Courier New", Font.BOLD, 30));
                buttonClicked.setText("O");
            }

            if (isWin()) {
                JOptionPane.showConfirmDialog(null, "Game Over.");
                resetButtons();
            }
            alternate++;
        }

        public boolean isWin() {
            if (check(0, 1) && check(1, 2)) //no need to put " == true" because the default check is for true
                return true;
            else if (check(3, 4) && check(4, 5))
                return true;
            else if (check(6, 7) && check(7, 8))
                return true;

                //vertical win check
            else if (check(0, 3) && check(3, 6))
                return true;
            else if (check(1, 4) && check(4, 7))
                return true;
            else if (check(2, 5) && check(5, 8))
                return true;

                //diagonal win check
            else if (check(0, 4) && check(4, 8))
                return true;
            else if (check(2, 4) && check(4, 6))
                return true;
            else
                return false;

        }

        public boolean check(int a, int b) {
            if (buttons[a].getText().equals(buttons[b].getText()) && !buttons[a].getText().equals(""))
                return true;
            else
                return false;
        }

    }

    public static void main(String[] args) {
        JFrame window = new JFrame("Tic-Tac-Toe");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.getContentPane().add(new TicTacToe());
        window.setBounds(300, 200, 300, 300);
        window.setVisible(true);
    }
}

