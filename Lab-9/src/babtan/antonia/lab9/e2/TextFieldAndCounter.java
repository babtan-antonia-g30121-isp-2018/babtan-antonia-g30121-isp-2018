package babtan.antonia.lab9.e2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextFieldAndCounter extends JFrame {

    static int count=0;
    JButton button;
    JTextField textField ;

    TextFieldAndCounter(){
        setTitle("Exercitiul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,200);
        setVisible(true);
    }
    public void  init(){
        this.setLayout(null);

        textField = new JTextField("0");
        textField.setBounds(10,25,80, 20);

        button = new JButton("Count");
        button.setBounds(10,75,80,20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField.setText(String.valueOf(++count));
            }
        });
        add(button);
        add(textField);
    }

    public static void main(String[] args) {
        TextFieldAndCounter t = new TextFieldAndCounter();
    }
}

