package babtan.antonia.lab5.e1;

public class Test {
    public static void main(String[] args) {

        Circle c1= new Circle("blue", true, 3.0);


        Square s1= new Square("red",true,7);
        Rectangle r1= new Rectangle("green", true, 2.0, 2.0);

        Shape Array[]={c1,s1,r1};
        for(int i=0;i<Array.length;i++){
            double Area = Array[i].getArea();
            System.out.println("Area: "+Area);
            double Perimeter = Array[i].getPerimeter();
            System.out.println("Perimeter: "+ Perimeter);
        }
    }
}