package babtan.antonia.lab5.e1;

abstract class Shape {
    protected String color = "red";
    protected boolean filled = true;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        String x;
        if (this.filled) {
            x = "filled";
        } else {
            x = "not filled";
        }
        return "A Shape with color of " + this.color + " and " + x;
    }

    abstract double getArea();
    abstract double getPerimeter();

}
