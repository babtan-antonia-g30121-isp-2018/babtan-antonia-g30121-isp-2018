package babtan.antonia.lab5.e2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private String imageType;

    public ProxyImage(String fileName, String ImageType) {
        this.fileName = fileName;
        this.imageType = ImageType;
    }

    @Override
    public void display() {
        if (imageType == "Real") {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        } else if (imageType == "Rotated") {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }


    }
}
