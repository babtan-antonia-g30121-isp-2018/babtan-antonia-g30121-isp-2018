package babtan.antonia.lab5.e2;

public class Test {
    public static void main(String[] args) {
        RotatedImage roI=new RotatedImage("Rotated Image");
        roI.display();
        ProxyImage prIre=new ProxyImage("Real Proxy Image","Real");
        prIre.display();
        ProxyImage prIro=new ProxyImage("Rotated Proxy Image","Rotated");
        prIro.display();
        RealImage reI=new RealImage("Real Image");
        reI.display();


    }
}
