package babtan.antonia.lab5.e3;

public class Sensor {
    private String location;

    public abstract int readValue();

    public String getLocation() {
        return location;
    }
}
