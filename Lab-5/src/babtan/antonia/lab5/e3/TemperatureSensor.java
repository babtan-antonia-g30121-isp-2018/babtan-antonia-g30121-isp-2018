package babtan.antonia.lab5.e3;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    Random r = new Random();

    //Override
    public int readValue() {
        return r.nextInt(101);
    }
}
