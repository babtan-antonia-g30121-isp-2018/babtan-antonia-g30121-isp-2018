package babtan.antonia.lab8.e4;


public class NoEvent extends Event {
    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}