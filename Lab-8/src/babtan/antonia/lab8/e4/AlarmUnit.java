package babtan.antonia.lab8.e4;


public class AlarmUnit {
    protected String owner;

    public AlarmUnit() {
    }

    public AlarmUnit(String owner) {
        this.owner = owner;
    }

    public void event(Event event) throws Exception {

        if (event.getType().equals(EventType.FIRE) && ((FireEvent)event).isSmoke() == true)
        {
            GSMUnit g = new GSMUnit();
            g.callOwner();
            throw new Exception("Fire in the house");
        }

    }
}