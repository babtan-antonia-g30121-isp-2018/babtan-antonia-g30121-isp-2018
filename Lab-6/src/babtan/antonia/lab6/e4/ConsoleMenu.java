package babtan.antonia.lab6.e4;

import java.util.Scanner;

public class ConsoleMenu {

    public static void main(String[] args)  {

        System.out.println("Hello!");

        Scanner scan = new Scanner(System.in);
        Dictionary d = new Dictionary();
        int option = 0;

        while(option <=4)
        {
            System.out.println("1.Add item \n2.Get value\n3.Get all words\n4.Get all definitions \n5.Exit");
            System.out.println("Enter option: ");
            option = scan.nextInt();
            switch (option)
            {
                case 1:
                {
                    System.out.println("Add word: \n");
                    Word w = new Word(scan.next());
                    System.out.println("Add definition: \n");
                    Definition d1 = new Definition(scan.next());
                    d.addWord(w,d1);
                    break;
                }
                case 2:
                {
                    System.out.println("Input key for word: ");
                    Word w = new Word(scan.next());
                    System.out.println("Definition is: " + d.getDefinition(w).getDescription());
                    break;
                }
                case 3:
                {
                    System.out.println("Getting all words: ");
                    d.getAllWords();
                    break;
                }
                case 4:
                    System.out.println("Get all definitions: ");
                    d.getAllDefinitions();
                    break;
                default:
                    break;
            }
        }



    }

}
