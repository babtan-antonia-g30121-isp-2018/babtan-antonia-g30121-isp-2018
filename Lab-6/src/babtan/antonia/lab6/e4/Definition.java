package babtan.antonia.lab6.e4;

public class Definition {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Definition(String description) {
        this.description = description;
    }

}
