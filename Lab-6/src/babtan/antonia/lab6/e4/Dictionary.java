package babtan.antonia.lab6.e4;
import java.util.HashMap;

public class Dictionary {

    private HashMap<Word,Definition> dictionary = new  HashMap();

    public void addWord(Word w, Definition d)
    {
        dictionary.put(w,d);
    }


    public Definition getDefinition(Word w)
    {

        for (Word k : dictionary.keySet())
        {
            if (k.getName().equals(w.getName()))
            {
                w = k;
                break;
            }
        }

        return dictionary.get(w);
    }

    public void getAllWords()
    {
        dictionary.keySet().stream().forEach(b-> System.out.println(b.getName()));
    }

    public void getAllDefinitions()
    {
        dictionary.values().stream().forEach(b-> System.out.println(b.getDescription()));
    }

}
