package babtan.antonia.lab6.e1;

public class Test {

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("abc",100);
        BankAccount b2 = new BankAccount("abc",100);

        b1.deposit(1000);
        b2.deposit(1000);

        System.out.println(b1.equals(b2));
        System.out.println(b1.hashCode());
        System.out.println(b2.hashCode());

    }

}
