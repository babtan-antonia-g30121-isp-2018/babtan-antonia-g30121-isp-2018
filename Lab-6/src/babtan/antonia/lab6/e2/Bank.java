package babtan.antonia.lab6.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Bank {

    private List accounts = new ArrayList<BankAccount>();

    public void addAccount(String owner,double balance)
    {
        accounts.add(new BankAccount(owner,balance));
    }

    public void printAccounts()
    {
        Collections.sort(accounts, new Comparator<BankAccount>() {
            public int compare(BankAccount b1, BankAccount b2)
            {
                return Double.compare(b1.getBalance(),b2.getBalance());
            }
        });

        accounts.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner()+" Balance: "+((BankAccount)b).getBalance()));
    }

    public void printAccounts(double minBalance, double maxBalance)
    {
        accounts = (List) accounts.stream().filter(b->((BankAccount)b).getBalance() >= minBalance && ((BankAccount)b).getBalance() <= maxBalance).collect(Collectors.toList());


        accounts.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner() +" Balance: "+ ((BankAccount)b).getBalance()));
    }

    public BankAccount getAccount(String owner)
    {
        return (BankAccount) accounts.stream().filter(b-> ((BankAccount)b).getOwner() == owner).findFirst().get();
    }

    public List getAllAccounts() {
        return accounts;
    }
}
