package babtan.antonia.lab6.e2;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance)
    {
        this.balance=balance;
        this.owner=owner;
    }
    public void withdraw(double amount) {
        this.balance = this.balance - amount;
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount acc = (BankAccount) obj;
            return balance == acc.balance && acc.owner.equals(owner);
        }else {
            return false;
        }
    }
    public int hashCode() {
        return (int)balance + owner.hashCode();
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }
}
