package babtan.antonia.lab6.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {

        Bank bank = new Bank();
        bank.addAccount("Mihai",240);
        bank.addAccount("George",370);
        bank.addAccount("Andrei",140);
        bank.addAccount("Alex",9700);

        bank.printAccounts();
        System.out.println("------------------------------");
        bank.printAccounts(200,300);
        System.out.println("------------------------------");
        System.out.println(bank.getAccount("Mihai").getBalance());

        System.out.println("------------------------------");

        List accounts_2 = (List) bank.getAllAccounts().stream().sorted((b1,b2)->((BankAccount)b1).getOwner().compareTo(((BankAccount)b2).getOwner())).collect(Collectors.toList());

        accounts_2.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner()+" Balance: "+((BankAccount)b).getBalance()));

    }
}
