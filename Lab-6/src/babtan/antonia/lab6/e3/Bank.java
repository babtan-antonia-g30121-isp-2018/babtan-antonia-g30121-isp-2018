package babtan.antonia.lab6.e3;

import java.util.*;
import java.util.stream.Collectors;


public class Bank {

    private TreeSet accounts = new TreeSet();

    public void addAccount(String owner, double balance) {

        accounts.add(new BankAccount(owner, balance));

    }

    public void printAccounts() {
        accounts.forEach(b -> System.out.println("Owner: " + ((BankAccount) b).getOwner() + " Balance: " + ((BankAccount) b).getBalance()));
    }

    public void printAccounts(double minBalance, double maxBalance) {
        TreeSet accounts_temp = (TreeSet) accounts.stream().filter(b -> ((BankAccount) b).getBalance() >= minBalance && ((BankAccount) b).getBalance() <= maxBalance).collect(Collectors.toCollection(TreeSet::new));

        System.out.println("Test");

        accounts_temp.forEach(b -> System.out.println("Owner: " + ((BankAccount) b).getOwner() + " Balance: " + ((BankAccount) b).getBalance()));
    }

    public BankAccount getAccount(String owner) {
        return (BankAccount) accounts.stream().filter(b -> ((BankAccount) b).getOwner() == owner).findFirst().get();

    }

    public TreeSet getAllAccounts() {
        return accounts;
    }
}
