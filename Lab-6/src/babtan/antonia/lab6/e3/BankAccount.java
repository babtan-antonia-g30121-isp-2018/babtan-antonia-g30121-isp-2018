package babtan.antonia.lab6.e3;

import java.util.Comparator;

public class BankAccount implements Comparable {


    private String owner;
    private double balance;



    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount)
    {
        if (this.balance <= amount)
        {
            System.out.println("Insufficient founds");
        }
        else
            this.balance -=amount;
    }
    public void deposit(double amount)
    {
        this.balance +=amount;
    }

    @Override
    public int compareTo(Object o) {
        return Double.compare(this.balance,((BankAccount)o).getBalance());
    }


}

