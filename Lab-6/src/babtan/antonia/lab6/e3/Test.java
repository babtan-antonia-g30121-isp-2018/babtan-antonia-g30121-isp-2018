package babtan.antonia.lab6.e3;

import java.util.Comparator;
import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {

        Bank bank = new Bank();
        bank.addAccount("Mihai",240);
        bank.addAccount("George",370);
        bank.addAccount("Andrei",140);
        bank.addAccount("Alex",9700);

        bank.printAccounts();
        System.out.println("------------------------");
        bank.printAccounts(200,300);
        System.out.println("------------------------");
        System.out.println(bank.getAccount("Mihai").getBalance());
        System.out.println("------------------------");


        Comparator<BankAccount> customComp = new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount o1, BankAccount o2) {
                return o1.getOwner().compareTo(o2.getOwner());
            }
        };
        TreeSet t1 = new TreeSet(customComp);
        t1.addAll(bank.getAllAccounts());
        t1.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner()+" Balance: "+ ((BankAccount)b).getBalance()));
    }
}
